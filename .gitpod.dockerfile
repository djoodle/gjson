FROM gitpod/workspace-full:latest

USER root

# Install dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends gcc clang make git \
    && apt-get clean && rm -rf /var/cache/apt/* \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/*

# Install v
RUN git clone https://github.com/vlang/v.git \
    && cd v \
    && make \
    && ./v symlink \
    && chown -R gitpod:gitpod /var/tmp/tcc \
    && chown -R gitpod:gitpod /tmp/vc \
    && chown -R gitpod:gitpod ~/v \
    && cd ../

USER gitpod

# Install vpkg as gitpod user (to not mess up v tmp files)
RUN git clone https://github.com/vpkg-project/vpkg.git vpkg \
    && cd vpkg \
    && v -prod . \
    && chmod a+x vpkg \
    && cd ../

USER root

RUN ln -s  /home/gitpod/vpkg/vpkg /usr/local/bin/vpkg
